import { Component, OnInit } from '@angular/core';
import { user } from "../user";
import { UserService } from '../user.service';
import { Location } from '@angular/common';
import { LoadChildren } from '@angular/router/src/config';

@Component({
  selector: 'app-componente',
  templateUrl: './componente.component.html',
  styleUrls: ['./componente.component.css']
})
export class ComponenteComponent implements OnInit {
  title="This is a component";
  user_list : user [];
  selected_user :user;
  constructor(
    private userService: UserService,
    private location : Location
  ) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(): void{
    //this.user_list = this.userService.getUsers();
    this.userService.getUsers().subscribe(users => this.user_list = users);
  }
  goBack(): void {
    this.location.back();
  }
}