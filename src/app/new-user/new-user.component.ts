import { Component, OnInit } from '@angular/core';
import { user,Luser } from "../user";
import { UserService } from '../user.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {
  Luser: Luser ={
    name: null,
    address: null,
    phone: null
  };

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(
  ) {
  }

  saveUser(): void{
    this.userService.postUser(this.Luser);
  }

}
