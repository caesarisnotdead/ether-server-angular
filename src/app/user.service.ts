import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { MessageService } from './message.service';
import { user,Luser,Puser,NPuser } from "./user";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class UserService {
  private _urlA: string = "http://10.10.1.207:3000/api/users";
  private _urlB: string = "http://10.10.1.207:3000/api/users/";
  private _urlGPuser: string = "http://10.10.1.209:3000/users";
  private _urlNPuser: string = "http://10.10.1.209:3000/users/register";
  

  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) { }

  private log(message: string){
    this.messageService.add("SYSTEM UPDATE: " + message);
  }

  getUsers(): Observable<user[]> {
    this.log("Loaded users");
    return this.http.get<user[]>(this._urlA);
  }

  getPusers(): Observable<Puser[]>{
    this.log("Loaded Users remote database");
    var x = this.http.get<Puser[]>(this._urlGPuser);
    console.log(x);
    return x;
  }

  getUser(id: number): Observable<user>{
    this.log(`Loaded user id ${id}`);
    return this.http.get<user>(this._urlB+id);
  }
  postUser(Luser: Luser){
    this.log(`Posted ${Luser} to database`);
    this.http.post(this._urlA,Luser).subscribe();
  }

  postPuser(Puser: NPuser){
    this.log(`Posted ${Puser.firstname} to remote database`);
    this.http.post(this._urlNPuser,Puser).subscribe();
  }
}