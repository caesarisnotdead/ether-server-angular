import { Component, OnInit, Input,ViewChild } from '@angular/core';
import { MatPaginator,MatTableDataSource,MatSort} from '@angular/material';
import { user } from "../user";
import { UserService } from '../user.service';

@Component({
  selector: 'app-table-user',
  templateUrl: './table-user.component.html',
  styleUrls: ['./table-user.component.css']
})

export class TableUserComponent implements OnInit {

  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatPaginator) paginator:MatPaginator;

  dataSource = new MatTableDataSource;
  displayedColumns = ['id','name','address','phone'];

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getUsers();
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  getUsers(): void{
    this.userService.getUsers().subscribe(users => this.dataSource.data = users);
  }

}