import { Component, OnInit,Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-dialog-test',
  templateUrl: './dialog-test.component.html',
  styleUrls: ['./dialog-test.component.css']
})
export class DialogTestComponent implements OnInit {
  animal:string = "derp";

  constructor(public dialog:MatDialog) { }

  ngOnInit() {
  }

  openDialog(){
    let dialogRef=this.dialog.open(dialog_example,{
      width:'250px',
      data:{animal:this.animal}
    })
  }
}


@Component({
  selector:'dialog_example',
  templateUrl: 'dialog_test.html'
})
export class dialog_example{

  constructor(
    public dialogRef:MatDialogRef<dialog_example>,
    @Inject(MAT_DIALOG_DATA) public data:any){}

    onNoClick(): void {
      this.dialogRef.close();
    }
}