import { Component } from '@angular/core';
import { navPoints } from './navElements';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my App';
  description = 'This is a test module.';

  navArray : navPoints[] = [
  {name:"Componente",reference:"ComponenteComponent",label:"Componente"},
  {name:"TablaUsuario", reference: "TableUserComponent",label:"Tabla de Usuarios"},
  {name:"NuevoUsuario",reference: "NewUserComponent",label:"Nuevo Usuario"},
  {name:"TablaPusuario",reference: "TablePuserComponent",label:"Tabla de Usuarios Remotos"},
  {name:"Bitacora",reference:"MessagesComponent",label:"Bitacora"},
  {name:"NuevoPUsuario",reference: "NewPuserComponent",label:"Nuevo Usuario Remoto"},
  {name:"DTest",reference:"DialogTestComponent",label:"Dialogo"}
  ];
}
