import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatSidenavModule,MatPaginatorModule ,MatTableModule,MatTableDataSource,
  MatSortModule, MatToolbarModule, MatIconModule, MatFormFieldModule, MatDialogModule, MatDialogRef,} from '@angular/material';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


import { AppComponent } from './app.component';
import { ComponenteComponent } from './componente/componente.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { TableUserComponent } from './table-user/table-user.component';
import { AppRoutingModule } from './/app-routing.module';
import { UserService } from './user.service'
import { MessagesComponent } from './messages/messages.component';
import { MessageService } from './message.service';
import { NewUserComponent } from './new-user/new-user.component';
import { TablePuserComponent } from './table-puser/table-puser.component';
import { NewPuserComponent } from './new-puser/new-puser.component';
import { DialogTestComponent,dialog_example } from './dialog-test/dialog-test.component';


@NgModule({
  declarations: [
    AppComponent,
    ComponenteComponent,
    EditUserComponent,
    TableUserComponent,
    MessagesComponent,
    NewUserComponent,
    TablePuserComponent,
    NewPuserComponent,
    DialogTestComponent,
    dialog_example
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MatTableModule,
    AppRoutingModule,
    HttpClientModule,
    MatSortModule,
    BrowserAnimationsModule,
    MatPaginatorModule,
    MatSidenavModule,
    MatIconModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatDialogModule
  ],
  entryComponents:[dialog_example,DialogTestComponent],
  providers: [UserService,MessageService],
  bootstrap: [AppComponent,dialog_example]
})
export class AppModule { }
