import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComponenteComponent } from './componente/componente.component';
import { TableUserComponent } from './table-user/table-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { NewUserComponent } from './new-user/new-user.component';
import { TablePuserComponent } from './table-puser/table-puser.component';
import { MessagesComponent } from './messages/messages.component';
import { NewPuserComponent } from './new-puser/new-puser.component';
import { DialogTestComponent } from './dialog-test/dialog-test.component';

const routes: Routes = [
  {path: '', redirectTo: '/TablaUsuario', pathMatch: 'full' },
  {path: 'Componente', component: ComponenteComponent},
  {path: 'TablaUsuario', component: TableUserComponent},
  {path: 'EditUsuario/:id', component: EditUserComponent},
  {path: 'NuevoUsuario', component: NewUserComponent},
  {path: 'TablaPusuario', component: TablePuserComponent},
  {path: 'Bitacora',component: MessagesComponent},
  {path: 'NuevoPUsuario',component: NewPuserComponent},
  {path: 'DTest',component: DialogTestComponent}

]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule,
  ],
})
export class AppRoutingModule { }

