import { Component, OnInit,ViewChild } from '@angular/core';
import { UserService } from '../user.service';
import { MatTableDataSource,MatPaginator,MatSort } from '@angular/material';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Puser } from '../user';

@Component({
  selector: 'app-table-puser',
  templateUrl: './table-puser.component.html',
  styleUrls: ['./table-puser.component.css']
})
export class TablePuserComponent implements OnInit {

  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatPaginator) paginator:MatPaginator;

  dataSource = new MatTableDataSource;
  lista_Usuarios: Puser;
  displayedColumns = ["id_usuario","firstname","lastname","email","password"];
  constructor(
    private http: HttpClient,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.getPusers();
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  getPusers(): void{
    this.userService.getPusers().subscribe(Pusers => this.dataSource.data = Pusers);
  }
}
