import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablePuserComponent } from './table-puser.component';

describe('TablePuserComponent', () => {
  let component: TablePuserComponent;
  let fixture: ComponentFixture<TablePuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablePuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablePuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
