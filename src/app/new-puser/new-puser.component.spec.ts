import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPuserComponent } from './new-puser.component';

describe('NewPuserComponent', () => {
  let component: NewPuserComponent;
  let fixture: ComponentFixture<NewPuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
