import { Component, OnInit } from '@angular/core';
import { user,Luser,NPuser } from "../user";
import { UserService } from '../user.service';

@Component({
  selector: 'app-new-puser',
  templateUrl: './new-puser.component.html',
  styleUrls: ['./new-puser.component.css']
})
export class NewPuserComponent implements OnInit {
  Puser: NPuser = {
    firstname:null,
    lastname:null,
    email:null,
    password:null
  }

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  saveNPuser():void{
    this.userService.postPuser(this.Puser);
  }

}
